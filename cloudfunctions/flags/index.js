// 云函数入口文件
const cloud = require('wx-server-sdk')



cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
  console.log(event)
  switch (event.action) {
    case 'add': {
      return addFlag(event)
    }
    case 'get': {
      return getFlags(event)
    }
  }
}
const db = cloud.database()

/**
 * 添加用户信息
 * @param {name} event 
 */
async function addFlag(event) {
  console.log(event)
  db.collection('ht_flags').add({
    data: {
      name: event.name
    }
  }).then(res => {
    return res
  }).catch(
    console.error
  )
}

/**
 * 获取所有标签
 */
async function getFlags(event) {
  return db.collection('ht_flags').where({}).get()  
}