// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  console.log(event)
  switch(event.action) {
    case 'add': {
      return addBlog(event, wxContext)
    }
    case 'edit': {

    }
    case 'get': {
      return getBlog(event, wxContext)
    }
    case 'del': {
      return delBlog(event, wxContext)
    }
    case 'all': {
      return getAllBlog(event, wxContext)
    }
  }
}

const db = cloud.database()
const $ = db.command.aggregate
const MAX_LIMIT = 20

async function addBlog (event, wxContent) {
  return db.collection('ht_blogs').add({
    data: {
      ...event.data,
      _openid: wxContent.OPENID,
      add_time: db.serverDate()
    }
  }).then(res => {
    return res
  })
}

/**
 * 获取自己的blog
 * @param {} event 
 * @param {*} wxContent 
 */
async function getBlog (event, wxContent) {

  return db.collection('ht_blogs').where({
    _openid: event.openid ? event.openid : wxContent.OPENID
  }).orderBy('add_time', 'desc').get().then(res => {
    return res.data
  })
}

/**
 * 获取所有blog
 * @param {} event 
 * @param {*} wxContent 
 */
async function getAllBlog (event, wxContent) {
  // const countResult = db.collection('ht_blogs').count()
  // const total = countResult.total
  // const batchTimes = Math.ceil(total / MAX_LIMIT)
  return db.collection('ht_blogs').aggregate().lookup({
    from: 'ht_users',
    localField: '_openid',
    foreignField: "_openid",
    as: 'userList'
  }).sort({
    add_time: -1
  }).skip(event.page * MAX_LIMIT).limit(MAX_LIMIT).end().then(res => {
    return res
  })
  // return db.collection('ht_blogs').skip(event.page * MAX_LIMIT).limit(MAX_LIMIT).get().then(res => {
  //   return res.data
  // })
}

/**
 * 删除日志
 */
async function delBlog (event, wxContent) {
  return db.collection('ht_blogs').doc(event.id).remove().then(res => {
    return res
  })
}