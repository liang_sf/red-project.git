// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const db = cloud.database()
  switch (event.keyWord) {
    case 'name': {
      return db.collection('ht_users').doc(event._id).update({
        data: {
          company_name: event.keyValue,
        },
        success: res => {
          return res
        }
      })
    }
    case 'no': {
      return db.collection('ht_users').doc(event._id).update({
        data: {
          company_code: event.keyValue,
        },
        success: res => {
          return res
        }
      })
    }
    case 'con': {
      return db.collection('ht_users').doc(event._id).update({
        data: {
          company_link_name: event.keyValue,
        },
        success: res => {
          return res
        }
      })
    }
    case 'phone': {
      return db.collection('ht_users').doc(event._id).update({
        data: {
          phone: event.keyValue,
        },
        success: res => {
          return res
        }
      })
    }
    case 'pics': {
      return db.collection('ht_users').doc(event._id).update({
        data: {
          company_card: event.keyValue,
        },
        success: res => {
          return res
        }
      })
    }
  }
}