// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const goodsId = ''
  const db = cloud.database()
  db.collection('ht_products').add({
    data: {
      enterpriseId: event.enterpriseId,
      goodsName: event.goodsName,
      goodsDesc: event.goodsDesc,
      goodsPrice: event.goodsPrice,
      goodsDed: event.goodsDed,
      goodsPicList: event.goodsPicList
    },
    success: res => {
      // 在返回结果中会包含新创建的记录的 _id
      this.setData({
        goodsId: res._id
      })
      wx.showToast({
        title: '新增记录成功',
      })
      console.log('[数据库] [新增记录] 成功，记录 _id: ', res._id)
    },
    fail: err => {
      wx.showToast({
        icon: 'none',
        title: '新增记录失败'
      })
      console.error('[数据库] [新增记录] 失败：', err)
    }
  })
  return goodsId
}