// pages/guide/index.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    avatarUrl: '../index/user-unlogin.png',
    userInfo: {},
    logged: false,
    selectType: 1,
    showPage: false,
    templateId: 'JJMQvMQMz-VXh2Ea9800j1dX7LSg1ixPFfPfqWduQHY',
    userData: {
      _openid: '',
      app_accounts: [
        {
          account_id: '',
          app: '抖音号',
          fans: 0,
          name: ''
        },
        {
          account_id: '',
          app: '快手好',
          fans: 0,
          name: ''
        },
        {
          account_id: '',
          app: '微视号',
          fans: 0,
          name: ''
        },
        {
          account_id: '',
          app: '淘宝号',
          fans: 0,
          name: ''
        },
        {
          account_id: '',
          app: '微信号',
          fans: 0,
          name: ''
        }
      ],
      company_card: [],
      company_code: '',
      company_link_name: '',
      company_name: '',
      flags: ['达人'],
      nike_name: '',
      phone: '',
      remark: '',
      type: 1,
      wx_name: '',
      avatarUrl: '',
      gender: '',
      city: '',
      country: '',
      province: ''
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (!wx.cloud) {
      wx.redirectTo({
        url: '../chooseLib/chooseLib',
      })
      return
    }

    // 获取用户信息2
    wx.getSetting({
      success: res => {
        console.log(res)
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              console.log(res)
              this.setData({
                avatarUrl: res.userInfo.avatarUrl,
                userInfo: res.userInfo
              })
            }
          })
        }
      }
    })


    this.onGetOpenid()
    // this.getUserInfo()
  },

  selectUser: function () {
    this.data.userData.type = 1
    app.globalData.userType = 1
    this.setData({
      selectType: 1,
      userData: this.data.userData
    })
  },

  selectCom: function () {
    this.data.userData.type = 2
    app.globalData.userType = 2
    this.setData({
      selectType: 2,
      userData: this.data.userData
    })
  },

  onGetOpenid: function() {
    // 调用云函数
    wx.cloud.callFunction({
      name: 'login',
      data: {},
      success: res => {
        console.log('[云函数] [login] user openid: ', res.result.openid)
        app.globalData.openid = res.result.openid
      },
      fail: err => {
        console.error('[云函数] [login] 调用失败', err)
      }
    })
  },

  onGetUserInfo: function(e) {

    console.log(e.detail)
    if (!this.data.logged && e.detail.userInfo) {
      let userRes = e.detail.userInfo
      this.data.userData.avatarUrl = userRes.avatarUrl
      this.data.userData.city = userRes.city
      this.data.userData.country = userRes.country
      this.data.userData.gender = userRes.gender
      this.data.userData.nike_name = userRes.nickName
      this.data.userData.province = userRes.province
      this.setData({
        logged: true,
        avatarUrl: e.detail.userInfo.avatarUrl,
        userInfo: e.detail.userInfo,
        userData: this.data.userData
      })
      wx.showLoading({
        title: '请稍等',
      })
      wx.cloud.callFunction({
        name: 'users',
        data: {
          action: 'add',
          data: this.data.userData
        }
      }).then(res => {
        wx.hideLoading()
        console.log(res)
        wx.switchTab({
          url: '../home/index',
        })
      })
    }
  },
  // 获取用户信息
  getUserInfo: function () {
    wx.cloud.callFunction({
      name: 'users',
      data: {
        action: 'get'
      }
    }).then(res => {
      if (res.result === null) {
        this.setData({
          showPage: true
        })
      } else {
        wx.switchTab({
          url: '../home/index',
        })
      }
    })
  }
})