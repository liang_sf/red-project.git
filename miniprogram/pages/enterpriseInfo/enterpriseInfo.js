// miniprogram/pages/enterpriseInfo/enterpriseInfo.js

Page({
  data: {
    enterpriseInfo: {},
    zzPicList: [],
    goodsList: []
  },

  onLoad: function (options) {
    let enterpriseId = options.enterpriseId
    console.log('enid:'+enterpriseId)
    if (enterpriseId) {
      //获取企业信息
      wx.cloud.callFunction({
        name: 'getEnterpriseInfo',
        data: {enterpriseId: enterpriseId},
        success: res => {
          // console.log(JSON.stringify(res.result))
          this.setData({
            enterpriseInfo: res.result.data[0],
          })
          // console.log(this.data.enterpriseInfo.company_code)
          if (this.data.enterpriseInfo.company_name !== '' && this.data.enterpriseInfo.company_name !== undefined) {
            this.setData({nameEn: true})
          }else {
            this.setData({nameEn: false})
          }
          if (this.data.enterpriseInfo.company_code !== '' && this.data.enterpriseInfo.company_code !== undefined) {
            this.setData({noEn: true})
          }else {
            this.setData({noEn: false})
          }
          if (this.data.enterpriseInfo.company_link_name !== '' && this.data.enterpriseInfo.company_link_name !== undefined) {
            this.setData({contractorEn: true})
          }else {
            this.setData({contractorEn: false})
          }
          if (this.data.enterpriseInfo.company_phone !== '' && this.data.enterpriseInfo.company_phone !== undefined) {
            this.setData({phoneEn: true})
          }else {
            this.setData({phoneEn: false})
          }
          if (this.data.enterpriseInfo.company_card.length > 0) {
            this.setData({zzPicList: this.data.enterpriseInfo.company_card})
          }
        },
        fail: err => {
          wx.showToast({
            icon: 'none',
            title: '调用失败',
          })
          console.error('[云函数] [sum] 调用失败：', err)
        }
      })
      //获取商品信息
      wx.cloud.callFunction({
        name: 'getGoods',
        data: {enterpriseId: enterpriseId},
        success: res => {
          // console.log(JSON.stringify(res.result))
          this.setData({
            goodsList: res.result.data
          })
        },
        fail: err => {
          wx.showToast({
            icon: 'none',
            title: '调用失败',
          })
          console.error('[云函数] [sum] 调用失败：', err)
        }
      })
    }
  },
})