// pages/user/addBlog.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    content: '',
    imagePath: '',
    imgList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  // 上传图片
  doUpload: function () {
    let that = this
    if (this.data.imgList.length>=6) {
      wx.showToast({
        title: '最多上传6张图片',
      })
    }
    // 选择图片
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: function (res) {

        wx.showLoading({
          title: '上传中',
        })

        const filePath = res.tempFilePaths[0]
        
        // 上传图片
        const cloudPath = 'my-image' + app.globalData.openid + Math.ceil(Math.random()*10000) + filePath.match(/\.[^.]+?$/)[0]
        wx.cloud.uploadFile({
          cloudPath,
          filePath,
          success: res => {
            // console.log('[上传文件] 成功：', res)
            
            // app.globalData.fileID = res.fileID
            // app.globalData.cloudPath = cloudPath
            // app.globalData.imagePath = filePath

            
            that.data.imgList.push(res.fileID)
            that.setData({
              // imagePath: res.fileID,
              imgList: that.data.imgList
            })
            console.log(that.data.imgList)
          },
          fail: e => {
            console.error('[上传文件] 失败：', e)
            wx.showToast({
              icon: 'none',
              title: '上传失败',
            })
          },
          complete: () => {
            wx.hideLoading()
          }
        })

      },
      fail: e => {
        console.error(e)
      }
    })
  },
  // 设置内容
  setContent: function (event) {
    this.setData({
      content: event.detail.value
    })
  },
  // 保存内容
  saveBlog: function () {

    


    wx.cloud.callFunction({
      name: 'blog',
      data: {
        action: 'add',
        data: {
          imgs: this.data.imgList,
          desc: this.data.content,
          clicks: 0
        }
      }
    }).then(res => {
      // console.log(res)
      wx.showToast({
        title: '发布成功',
      })
      setTimeout(function () {
        wx.redirectTo({
          url: './user',
        })
      }, 500)
      
    })
  }
})