// pages/user/flag.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    flagList: [],
    selectFlagList: [],
    baseInfo: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getFlags()
    this.getUser()
  },
  onShow: function () {
    
  },
  // 获取用户信息
  getUser: function () {
    wx.cloud.callFunction({
      name: 'users',
      data: {
        action: 'get'
      }
    }).then(res => {
      this.setData({
        baseInfo: res.result,
        selectFlagList: res.result.flags
      })
    })
  },
  /**
   * 保存
   */
  saveFlag: function () {
    wx.cloud.callFunction({
      name: 'users',
      data: {
        action: 'edit',
        id: this.data.baseInfo._id,
        data: {
          flags: this.data.selectFlagList
        }
      }
    }).then(res => {
      wx.redirectTo({
        url: './user',
      })
      
    })
    
  },
  addFlag: function () {
    
    wx.cloud.callFunction({
      name: 'flags',
      data: {
        action: 'add',
        name: '鞋子'
      }
    }).then(res => {
      console.log(res)
    })
  },
  getFlags: function () {
    wx.cloud.callFunction({
      name: 'flags',
      data: {
        action: 'get'
      }
    }).then(res => {
      this.setData({
        flagList: res.result.data
      })
    })
  },
  // 选择标签
  selectFlag: function (event) {
    if (this.data.selectFlagList.length>=5) {
      wx.showToast({
        title: '最多选择5个标签',
      })
      return false
    }
    let item = event.target.dataset
    if (this.data.selectFlagList.includes(item.name)) {

    } else {
      this.data.selectFlagList.push(item.name)
      this.setData({
        selectFlagList: this.data.selectFlagList
      })
    }
  },
  // 删除标签
  deleteFlag: function (event) {
    let index = event.target.dataset.index
    console.log(index)
    this.data.selectFlagList.splice(index,1)
    this.setData({
      selectFlagList: this.data.selectFlagList
    })
   
  }
})