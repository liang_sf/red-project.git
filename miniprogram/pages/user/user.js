// miniprogram/pages/user/user.js.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    avatarUrl: '../../images/user-unlogin.png',
    userInfo: {},
    logged: false,
    takeSession: false,
    requestResult: '',
    baseInfo: {},
    app_accounts: {},
    blogList: [],
    isCurrentUser: false,
    // app_accounts: [
    //   {
    //     name: '分提莫',
    //     app: '抖音号',
    //     account_id: 'xxxdfsf',
    //     fans: '5000'
    //   },
    //   {
    //     name: '分提莫',
    //     app: '快手号',
    //     account_id: 'xxxdfsf',
    //     fans: '5000'
    //   },
    //   {
    //     name: '分提莫',
    //     app: '微视号',
    //     account_id: 'xxxdfsf',
    //     fans: '5000'
    //   },
    //   {
    //     name: '分提莫',
    //     app: '淘宝号',
    //     account_id: 'xxxdfsf',
    //     fans: '5000'
    //   },
    //   {
    //     name: '分提莫',
    //     app: '微信号',
    //     account_id: 'xxxdfsf',
    //     fans: '5000'
    //   }
    // ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (!wx.cloud) {
      wx.redirectTo({
        url: '../chooseLib/chooseLib',
      })
      return
    }

    // 获取用户信息2
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              this.setData({
                avatarUrl: res.userInfo.avatarUrl,
                userInfo: res.userInfo
              })
            }
          })
        }
      }
    })
    

    this.getUser()
    this.getBlogs()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // this.getTabBar().setData({
    //   selected: 3  //这个数字是当前页面在tabBar中list数组的索引
    // })
  },
  // 获取用户信息
  getUser: function () {
    wx.cloud.callFunction({
      name: 'users',
      data: {
        action: 'get'
      }
    }).then(res => {
      this.setData({
        baseInfo: res.result,
        app_accounts: res.result.app_accounts
      })
    })
  },
  /**
   * 点击编辑标签
   */
  editFlag: function () {
    wx.redirectTo({
      url: './flag',
    })
  },
  // 编辑app账号列表
  seveFans: function (event) {
    console.log(event)
    if (event.target.dataset.type == 'name') {
      this.data.baseInfo.app_accounts[event.target.dataset.index].name = event.detail.value
    }
    if (event.target.dataset.type == 'fans') {
      this.data.baseInfo.app_accounts[event.target.dataset.index].fans = parseInt(event.detail.value)
    }

    wx.cloud.callFunction({
      name: 'users',
      data: {
        action: 'edit',
        id: this.data.baseInfo._id,
        data: {
          app_accounts: this.data.baseInfo.app_accounts
        }
      }
    })
  },
  // 保存备注信息
  saveRemark: function (event) {
    this.data.baseInfo.remark = event.detail.value
    wx.cloud.callFunction({
      name: 'users',
      data: {
        action: 'edit',
        id: this.data.baseInfo._id,
        data: {
          remark: this.data.baseInfo.remark
        }
      }
    })
  },
  // 跳转至添加blog
  addBlog: function () {
    wx.redirectTo({
      url: './addBlog',
    })
  },
  // 获取bolg列表
  getBlogs: function () {
    wx.cloud.callFunction({
      name: 'blog',
      data: {
        action: 'get'
      }
    }).then(res => {
      this.setData({
        blogList: res.result
      })
    })
  },
  // 删除blog
  delBlog: function (event) {
    let that = this
    wx.showModal({
      title: '提示',
      content: '确定删除吗？',
      success: function (res) {
        if (res.confirm) {
          wx.cloud.callFunction({
            name: 'blog',
            data: {
              action: 'del',
              id: event.target.dataset.id
            }
          }).then(res => {
            wx.showToast({
              title: '删除成功！',
            })
            that.data.blogList.splice(event.target.dataset.index, 1)
            that.setData({
              blogList: that.data.blogList
            })
          })
        }
      }
    })
  },
  // 查看图片
  showImg:function (e) {
    console.log(e)
    wx.previewImage({
      urls: this.data.blogList[e.currentTarget.dataset.index].imgs,
      current: e.currentTarget.dataset.imgIndex
    })
  }
  
})