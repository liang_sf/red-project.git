// miniprogram/pages/enterpriseCenter/enterpriseCenter.js
const app = getApp()

Page({
  data: {
    avatarUrl: '../../images/user-unlogin.png',
    nickName: '',
    enterpriseId: '',
    // enterpriseName: '',
    // enterpriseNo: '',
    // enterpriseContractor: '',
    // enterprisePhone: '',
    nameEn: false,
    noEn: false,
    contractorEn: false,
    phoneEn: false,
    userInfo: {},
    enterpriseInfo: {},
    logged: false,
    takeSession: false,
    requestResult: '',
    zzPicList: [
      // {"picSrc":"http://zyp.cwkz.cn/img/txq2.fb8ec515.jpg"}
    ],
    goodsList: [
      // {
      //   "goodsName": "阳澄湖大闸蟹",
      //   "goodsDesc": "个大黄满",
      //   "goodsPrice": "30/只",
      //   "goodsDed": "8%",
      //   "goodsPicList": [
      //     {"goodsPicSrc": "http://cwgj.12366.com/static/system/login/image/icon_fwy.png"},
      //     {"goodsPicSrc": "http://cwgj.12366.com/static/system/login/image/icon_znjz.png"},
      //     {"goodsPicSrc": "http://cwgj.12366.com/static/system/login/image/icon_kjzd.png"},
      //   ]
      // }
    ]
  },

  onLoad: function (options) {
    // 企业id就是openId
    if (app.globalData.openid) {
      this.setData({
        enterpriseId: app.globalData.openid
      })
    }

    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              this.setData({
                avatarUrl: res.userInfo.avatarUrl,
                nickName: res.userInfo.nickName,
                userInfo: res.userInfo
              })
            }
          })
        }
      }
    })
    
    //获取企业信息
    if (this.data.enterpriseId) {
      wx.cloud.callFunction({
        name: 'getEnterpriseInfo',
        data: {enterpriseId: this.data.enterpriseId},
        success: res => {
          // console.log(JSON.stringify(res.result))
          this.setData({
            enterpriseInfo: res.result.data[0],
          })
          // console.log(this.data.enterpriseInfo.company_code)
          if (this.data.enterpriseInfo.company_name !== '' && this.data.enterpriseInfo.company_name !== undefined) {
            this.setData({nameEn: true})
          }else {
            this.setData({nameEn: false})
          }
          if (this.data.enterpriseInfo.company_code !== '' && this.data.enterpriseInfo.company_code !== undefined) {
            this.setData({noEn: true})
          }else {
            this.setData({noEn: false})
          }
          if (this.data.enterpriseInfo.company_link_name !== '' && this.data.enterpriseInfo.company_link_name !== undefined) {
            this.setData({contractorEn: true})
          }else {
            this.setData({contractorEn: false})
          }
          if (this.data.enterpriseInfo.company_phone !== '' && this.data.enterpriseInfo.company_phone !== undefined) {
            this.setData({phoneEn: true})
          }else {
            this.setData({phoneEn: false})
          }
          if (this.data.enterpriseInfo.company_card.length > 0) {
            this.setData({zzPicList: this.data.enterpriseInfo.company_card})
          }
        },
        fail: err => {
          wx.showToast({
            icon: 'none',
            title: '调用失败',
          })
          console.error('[云函数] [sum] 调用失败：', err)
        }
      })
    }
  },

  onShow: function() {
    // 每次展现都刷新一下商品列表
    if (this.data.enterpriseId) {
      wx.cloud.callFunction({
        name: 'getGoods',
        data: {enterpriseId: this.data.enterpriseId},
        success: res => {
          // console.log(JSON.stringify(res.result))
          this.setData({
            goodsList: res.result.data
          })
        },
        fail: err => {
          wx.showToast({
            icon: 'none',
            title: '调用失败',
          })
          console.error('[云函数] [sum] 调用失败：', err)
        }
      })
    }
  },

  onAddZZ: function () {
    let that = this
    // 选择图片
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: function (res) {
        wx.showLoading({
          title: '上传中',
        })
        const filePath = res.tempFilePaths[0]
        const fileName = 'ZZPic' + new Date() . getTime() + filePath.match(/\.[^.]+?$/)[0]
        const cloudPath = fileName
        // 上传图片
        wx.cloud.uploadFile({
          cloudPath,
          filePath,
          fileName,
          success: res => {
            that.setData({
              zzPicList: [
                ...that.data.zzPicList,
                { "picSrc": res.fileID }
              ]
            })
            that.updateEnterpriseInfo({keyWord: 'pics', keyValue: that.data.zzPicList})
            console.log('[上传文件] 成功：', res)
            // console.log('cloudPath：', cloudPath)
            // console.log('filePath：', filePath)
            // console.log('fileName：', fileName)
            console.log(that.data.zzPicList)
          },
          fail: e => {
            console.error('[上传文件] 失败：', e)
            wx.showToast({
              icon: 'none',
              title: '上传失败',
            })
          },
          complete: () => {
            wx.hideLoading()
          }
        })
      },
      fail: e => {
        console.error(e)
      }
    })
  },

  onEdit: function(event) {
    switch (event.currentTarget.dataset.word) {
      case 'name': {
        return this.setData({ nameEn: false })
      } 
      case 'no': {
        return this.setData({ noEn: false })
      } 
      case 'con': {
        return this.setData({ contractorEn: false })
      } 
      case 'phone': {
        return this.setData({ phoneEn: false })
      } 
    }
  },

  setState: function(event) {
    switch (event.currentTarget.dataset.word) {
      case 'name': {
        return this.setData({ nameEn: true })
      } 
      case 'no': {
        return this.setData({ noEn: true })
      } 
      case 'con': {
        return this.setData({ contractorEn: true })
      } 
      case 'phone': {
        return this.setData({ phoneEn: true })
      } 
    }
  },

  updateEnterpriseInfo: function(options) {
    console.log(options)
    wx.cloud.callFunction({
      name: 'updateEnterpriseInfo',
      data: {
        _id: this.data.enterpriseInfo._id, 
        keyWord: options.keyWord, 
        keyValue: options.keyValue,
      },
      success: res => {
        console.log(JSON.stringify(res.result))
      },
      fail: err => {
        console.error('[云函数] [updateEnterpriseInfo] 调用失败：', err)
      }
    })
  },

  onChange: function(event) {
    console.log(event)
    this.updateEnterpriseInfo({keyWord: event.currentTarget.dataset.word, keyValue: event.detail.value})
    if (event.detail.value != '' && event.detail.value != undefined) {
      this.setState(event)
    } 
  },

  onAddGoods: function() {
    app.globalData.enterpriseId = this.data.enterpriseId    
    wx.navigateTo({
      url: '../goodsAdd/goodsAdd'
    })
  }
})