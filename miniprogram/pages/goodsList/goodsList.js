// miniprogram/pages/goodsList/goodsList.js
Page({
  data: {
    goodsList: [],
    noneData: false
  },

  onLoad: function (options) {
    wx.cloud.callFunction({
      name: 'getGoods',
      data: {enterpriseId: 'getAll'},
      success: res => {
        console.log(JSON.stringify(res.result))
        if (res.result.data.length > 0){
          this.setData({
            goodsList: res.result.data,
            noneData: false
          })
        }else{
          this.setData({
            noneData: true
          })
        }
        console.log(this.data.goodsList)
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '调用失败',
        })
        this.setData({
          noneData: true
        })
        console.error('[云函数] [sum] 调用失败：', err)
      }
    })
  },

  onShow: function () {
    this.getTabBar().setData({
      selected: 2  //这个数字是当前页面在tabBar中list数组的索引
    })
  },

  onGoodsDetial: function (e) {
    let goodsId = e.currentTarget.dataset.goodsid
    wx.navigateTo({
      url: '../goodsDetial/goodsDetial?goodsId=' + goodsId
    })
  },

  onGetGoods: function (e) {
    let enterpriseId = e.currentTarget.dataset.enterpriseid
    wx.navigateTo({
      url: '../enterpriseInfo/enterpriseInfo?enterpriseId=' + enterpriseId
    })
  }
})
