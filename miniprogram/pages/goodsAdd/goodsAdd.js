// miniprogram/pages/goodsAdd/goodsAdd.js
const app = getApp()

Page({
  data: {
    goodsName: '',
    goodsDesc: '',
    goodsPrice: '',
    goodsDed: '',
    goodsPicList: []
  },

  onNameInput: function (e){
    this.setData({
      goodsName: e.detail.value
    })
  },

  onDescInput: function (e){
    this.setData({
      goodsDesc: e.detail.value
    })
  },

  onPriceInput: function (e){
    this.setData({
      goodsPrice: e.detail.value
    })
  },

  onDedInput: function (e){
    this.setData({
      goodsDed: e.detail.value
    })
  },

  onAddPic: function () {
    let that = this
    // 选择图片
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: function (res) {
        wx.showLoading({
          title: '上传中',
        })
        const filePath = res.tempFilePaths[0]
        const fileName = 'GoodsPic' + new Date() . getTime() + filePath.match(/\.[^.]+?$/)[0]
        const cloudPath = fileName
        // 上传图片
        wx.cloud.uploadFile({
          cloudPath,
          filePath,
          fileName,
          success: res => {
            that.setData({
              goodsPicList: [
                ...that.data.goodsPicList,
                { "goodsPicSrc": res.fileID }
              ]
            })
            console.log('[上传文件] 成功：', res)
            // console.log('cloudPath：', cloudPath)
            // console.log('filePath：', filePath)
            // console.log('fileName：', fileName)
            console.log(that.data.goodsPicList)
          },
          fail: e => {
            console.error('[上传文件] 失败：', e)
            wx.showToast({
              icon: 'none',
              title: '上传失败',
            })
          },
          complete: () => {
            wx.hideLoading()
          }
        })
      },
      fail: e => {
        console.error(e)
      }
    })
  },

  onSaveGoods: function () {
    wx.cloud.callFunction({
      name: 'addGoods',
      data: {
        enterpriseId: app.globalData.enterpriseId,
        goodsName: this.data.goodsName,
        goodsDesc: this.data.goodsDesc,
        goodsPrice: this.data.goodsPrice,
        goodsDed: this.data.goodsDed,
        goodsPicList: this.data.goodsPicList
      },
      success: res => {
        wx.showToast({
          title: '商品增加成功！',
        })
        console.log(JSON.stringify(res.result))
        wx.navigateBack({//返回
          delta: 1
        })
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '调用失败',
        })
        console.error('[云函数] [sum] 调用失败：', err)
      }
    })
  }
})